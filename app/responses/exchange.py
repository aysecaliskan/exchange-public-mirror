"""
Exchange Response Module
"""
from pydantic import BaseModel
from typing import List, Optional

class CurrencyExchange(BaseModel):
    retail: float
    purchase: float
    name: str


class ExchangeResponse(BaseModel):
    """
    Exchange Response
    """
    currencies: List[CurrencyExchange] = list()



class GAUExchange(CurrencyExchange):
    name = 'GAU'

class USDExchange(CurrencyExchange):
    name = 'USD'

class EURExchange(CurrencyExchange):
    name = 'EUR'
