from app.responses.exchange import ExchangeResponse
from app.services.enpara import EnparaCrawler
from fastapi import FastAPI
from starlette.responses import Response
from datetime import datetime
from .routers import enpara

app = FastAPI()

@app.get(
    path="/metrics/",
    tags=["exchange", "enpara", "prometheus"],
    )
async def metrics():
    crawler = EnparaCrawler()
    response = ExchangeResponse(
        currencies=crawler.crawl()
    )
    res = ''
    ts = int(float(datetime.now().timestamp()) * 1000)
    for currency in response.currencies:
        res += f"{currency.name.lower()}_retail"+'{provider="enpara"} '+f"{currency.retail} {ts}\n"
        res += f"{currency.name.lower()}_purchase"+'{provider="enpara"} '+f"{currency.purchase} {ts}\n"
    return Response(res, 200, headers={'content-type': 'text/plain'})

app.include_router(enpara.router, prefix='/enpara')
