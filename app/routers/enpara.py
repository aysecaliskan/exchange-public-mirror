"""
Enpara Exchange Module
"""
from fastapi import APIRouter
from app.responses.exchange import ExchangeResponse, GAUExchange, USDExchange, EURExchange
from app.services.enpara import EnparaCrawler

router = APIRouter()

@router.get(
    path="/exchange/",
    tags=["exchange", "enpara"],
    response_model=ExchangeResponse
    )
async def read_exchange():
    """
    Enpara Exchange
    """
    crawler = EnparaCrawler()

    return ExchangeResponse(
        currencies=crawler.crawl()
    )

@router.get(
    path="/exchange/gau",
    tags=["exchange", "enpara", "gau"],
    response_model=GAUExchange
    )
async def read_gau_exchange():
    """
    Enpara GAU Exchange
    """
    crawler = EnparaCrawler()
    return crawler.crawl_gau()

@router.get(
    path="/exchange/usd",
    tags=["exchange", "enpara", "usd"],
    response_model=USDExchange
    )
async def read_usd_exchange():
    """
    Enpara USD Exchange
    """
    crawler = EnparaCrawler()
    return crawler.crawl_usd()


@router.get(
    path="/exchange/eur",
    tags=["exchange", "enpara", "eur"],
    response_model=EURExchange
    )
async def read_eur_exchange():
    """
    Enpara EUR Exchange
    """
    crawler = EnparaCrawler()
    return crawler.crawl_eur()

