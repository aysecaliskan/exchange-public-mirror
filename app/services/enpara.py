import requests
from bs4 import BeautifulSoup
from bs4.element import Tag, ResultSet
from typing import List
from app.responses.exchange import CurrencyExchange, EURExchange, GAUExchange, USDExchange

class EnparaCrawler(object):
    """
    Enpara Crawler
    """

    URL = 'https://www.qnbfinansbank.enpara.com/hesaplar/doviz-ve-altin-kurlari'

    def __init__(self):
        self.__bs = None
        self.__get_page()

    def _replace_point(self, value: str, pointing=','):
        if pointing == ',':
            return value.replace('.', '').replace(',', '.')
        return value.replace(',', '')

    def _make_float(self, value: str) -> float:
        try:
            return float(self._replace_point(value.split(' ')[0]))
        except Exception as e:
            print(e)
            return -1

    def __get_page(self) -> BeautifulSoup:
        if self.__bs is None:
            response = requests.get(self.URL)
            self.__bs = BeautifulSoup(response.content, 'html.parser')
        return self.__bs

    def __base_div_parser(self, row_index: int) -> Tag or None:
        divs = self.__bs.findAll("div", {"class": "enpara-gold-exchange-rates__table-item"})
        return divs[row_index] if len(divs) > row_index else None
    
    def __base_span_parser(self, div: Tag) -> dict:
        spans = div.findChildren('span')
        return {
            'retail' : self._make_float(spans[1].text),
            'purchase' : self._make_float(spans[2].text)
        }

    def crawl_gau(self) -> GAUExchange:
        div = self.__base_div_parser(2)
        return GAUExchange(**self.__base_span_parser(div))
    
    def crawl_usd(self) -> USDExchange:
        div = self.__base_div_parser(0)
        return USDExchange(**self.__base_span_parser(div))

    def crawl_eur(self) -> EURExchange:
        div = self.__base_div_parser(1)
        return EURExchange(**self.__base_span_parser(div))

    def crawl(self) -> List[CurrencyExchange]:
        currencies : List[CurrencyExchange] = list()
        try:
            currencies.append(self.crawl_gau())
            currencies.append(self.crawl_eur())
            currencies.append(self.crawl_usd())
        except Exception as _:
            pass
        return currencies
